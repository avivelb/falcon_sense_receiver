import _thread
import logging
import signal
import socket
import sys
import time
import datetime
import json
import os
import RPi.GPIO as GPIO
from rpi_rf import RFDevice
import Adafruit_DHT
PWD = "/mnt/falconsense"
LOGFILE = "{}/logs/{}.log".format(PWD,datetime.datetime.now().strftime("%d-%m-%Y"))
logging.basicConfig(filename=LOGFILE,
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.INFO)
logging.getLogger().addHandler(logging.StreamHandler())
os.chdir(PWD)
class Client:
    def __init__(self, hostname="192.168.1.4", port=4005):
        self.hostname = hostname
        self.port = port
        self.client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect(self):
        connected = False
        seconds_start = 1
        while not connected:
            try:
                self.client_sock.connect((self.hostname, self.port))
            except OSError:
                logging.info(sys.exc_info()[0])
                time.sleep(seconds_start)
                seconds_start += 1
            finally:
                connected = True

    def send(self, msg):
        try:
            self.client_sock.send(msg.encode("UTF-8"))
        except (BrokenPipeError,OSError):
            self.exithandler()

    def recv(self):
        msg = self.client_sock.recv(4096).decode("UTF-8")
        if msg == "":
            self.exithandler()
        return msg

    def close(self):
        self.client_sock.close()

    def exithandler(self):
        logging.info("Broken Pipe Error ,Respawning in 15 seconds")
        rfdevice.cleanup()
        GPIO.cleanup()
        self.close()
        os._exit(1)


client = Client()
client_lock = _thread.allocate_lock()
write_lock = _thread.allocate_lock()
curr_states_lock = _thread.allocate_lock()
####Startof-Handlers####
rfdevice = None
gpio_to_pin = {2: 3, 3: 5, 4: 7,
               5: 29, 6: 31, 7: 26,
               8: 24, 9: 21, 10: 19,
               11: 23, 12: 32, 13: 33,
               14: 8, 15: 10, 16: 36,
               17: 11, 18: 12, 19: 35,
               20: 38, 21: 40, 22: 15,
               23: 16, 24: 18, 25: 22,
               26: 37, 27: 13}

DHT_SENSOR = Adafruit_DHT.DHT22
DHT_PIN = 20
humidity, temperature = 0, 0
last_detected = {}





def signal_handler(signal, frame):
    global client
    logging.info("Caught Ctrl+C, shutting down...")
    rfdevice.cleanup()
    GPIO.cleanup()
    client.close()
    sys.exit(0)


def get_file_contents(filename):
    with open(filename, "r") as file:
        return file.buffer.read()


def write_to_file(filename, buffer):
    with write_lock:
        with open(filename, "w") as file:
            return file.write(buffer)


####Endof-Handlers####

PHYSICAL_SENSORS_JSON_FILE = "physical_sensors.json"
WIRELESS_SENSORS_JSON_FILE = "wireless_sensors.json"
WIRELESS_GPIO = 4
PHYSICAL_GPIOS = tuple(json.loads(get_file_contents(PHYSICAL_SENSORS_JSON_FILE)))
ALARM_GPIO = 12
COOLING_FANS_GPIO = 16
WIRELESS_LENGTHS = tuple(json.loads(get_file_contents(WIRELESS_SENSORS_JSON_FILE)))
PROTOCOL_ADD_PHYSICAL_SENSOR = "ADDPHYS"
PROTOCOL_ADD_WIRELESS_SENSOR = "ADDWIRL"
PROTOCOL_DEL_PHYSICAL_SENSOR = "DELPHYS"
PROTOCOL_DEL_WIRELESS_SENSOR = "DELWIRL"
PROTOCOL_ENABLE_ALARM = "ENALARM"
PROTOCOL_DISABLE_ALARM = "DISALARM"
PROTOCOL_DELIMITER = "#"

rfdevice = RFDevice(WIRELESS_GPIO)
rfdevice.enable_rx()
MAX_TEMPERATURE = 32.1
MAX_HUMIDITY = 49

MIN_TEMPERATURE = 30.4
MIN_HUMIDITY = 44.6
timestamp = None
curr_states = dict()
logging.info("Listening for codes on GPIO " + str(WIRELESS_GPIO))
# Initializing physical sensors GPIOs
GPIO.setup(PHYSICAL_GPIOS, GPIO.IN, pull_up_down=GPIO.PUD_UP)
# Initializing alarm and cooling fan GPIOs
GPIO.setup((ALARM_GPIO, COOLING_FANS_GPIO), GPIO.OUT)


####Endof-Constants and initiallizable stuff####

####Startof-threads####
def client_recv_updates():
    global client
    global PHYSICAL_GPIOS
    global curr_states
    while True:
        msg = client.recv()
        spl = msg.split(PROTOCOL_DELIMITER)
        request = spl[0]
        logging.info("Got message {}".format(msg))
        # Add physical sensor
        if request == PROTOCOL_ADD_PHYSICAL_SENSOR:
            sensors = json.loads(get_file_contents(PHYSICAL_SENSORS_JSON_FILE))
            potential_add = int(spl[1])
            if potential_add not in sensors:
                sensors.append(potential_add)
                write_to_file(PHYSICAL_SENSORS_JSON_FILE, json.dumps(sensors))
                PHYSICAL_GPIOS = tuple(json.loads(get_file_contents(PHYSICAL_SENSORS_JSON_FILE)))
                GPIO.setup(PHYSICAL_GPIOS, GPIO.IN, pull_up_down=GPIO.PUD_UP)
                with curr_states_lock:
                    for gpio in PHYSICAL_GPIOS:
                        curr_states[gpio] = False
        # Delete physical sensor
        elif request == PROTOCOL_DEL_PHYSICAL_SENSOR:
            sensors = json.loads(get_file_contents(PHYSICAL_SENSORS_JSON_FILE))
            potential_del = int(spl[1])
            if potential_del in sensors:
                sensors.remove(potential_del)
                write_to_file(PHYSICAL_SENSORS_JSON_FILE, json.dumps(sensors))
                PHYSICAL_GPIOS = tuple(json.loads(get_file_contents(PHYSICAL_SENSORS_JSON_FILE)))
                GPIO.setup(PHYSICAL_GPIOS, GPIO.IN, pull_up_down=GPIO.PUD_UP)
                with curr_states_lock:
                    for gpio in PHYSICAL_GPIOS:
                        curr_states[gpio] = False
        # Add wireless sensor
        elif request == PROTOCOL_ADD_WIRELESS_SENSOR:
            sensors = json.loads(get_file_contents(WIRELESS_SENSORS_JSON_FILE))
            potential_add = int(spl[1])
            if potential_add not in sensors:
                sensors.append(potential_add)
                write_to_file(WIRELESS_SENSORS_JSON_FILE, json.dumps(sensors))
        # Delete wireless sensor
        elif request == PROTOCOL_DEL_WIRELESS_SENSOR:
            sensors = json.loads(get_file_contents(WIRELESS_SENSORS_JSON_FILE))
            potential_del = int(spl[1])
            if potential_del in sensors:
                sensors.remove(potential_del)
                write_to_file(WIRELESS_SENSORS_JSON_FILE, json.dumps(sensors))

        # Disable and enable alarm
        elif request == PROTOCOL_ENABLE_ALARM:
            GPIO.output(ALARM_GPIO, 1)

        elif request == PROTOCOL_DISABLE_ALARM:
            GPIO.output(ALARM_GPIO, 0)


def physical_recv():
    global client
    global PHYSICAL_GPIOS
    global curr_states
    # Initializing current states dict for each physical gpio

    with curr_states_lock:
        for gpio in PHYSICAL_GPIOS:
            curr_states[gpio] = False

    while True:
        for gpio in PHYSICAL_GPIOS:
            input = GPIO.input(gpio)
            # print("GPIO {} is: {}".format(gpio, input))
            if bool(input) != curr_states[gpio]:
                curr_states[gpio] = not curr_states[gpio]
                if curr_states[gpio]:
                    if gpio not in last_detected.keys() or \
                            (datetime.datetime.now() - last_detected[gpio]).total_seconds() >= 30:
                        last_detected[gpio] = datetime.datetime.now()
                        logging.info("Physical detection: {} on GPIO: {}".format(curr_states[gpio], gpio))
                        with client_lock:
                            client.send("PHYS#{}#{}".format(gpio, gpio_to_pin[gpio]))
        PHYSICAL_GPIOS = PHYSICAL_GPIOS
        time.sleep(0.3)


def wireless_recv():
    global timestamp
    global rfdevice
    global last_detected
    global client
    while True:
        if rfdevice.rx_code_timestamp != timestamp:
            timestamp = rfdevice.rx_code_timestamp
            if rfdevice.rx_code not in last_detected.keys() or \
                    (datetime.datetime.now() - last_detected[rfdevice.rx_code]).total_seconds() >= 30:
                last_detected[rfdevice.rx_code] = datetime.datetime.now()
                logging.info(str(rfdevice.rx_code) +
                             " [pulselength " + str(rfdevice.rx_pulselength) +
                             ", protocol " + str(rfdevice.rx_proto) + "]")
                with client_lock:
                    client.send("WIRL#{}#{}".format(WIRELESS_GPIO, rfdevice.rx_code))
        time.sleep(1)


def temperature_handler():
    global humidity
    global temperature
    is_cooling = False
    while True:
        humidity, temperature = Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)
        if humidity is not None and temperature is not None:
            if (temperature >= MAX_TEMPERATURE) and not is_cooling:
                logging.info("True: Temp={0:0.1f}*C  Humidity={1:0.1f}%".format(temperature, humidity))
                is_cooling = True
                GPIO.output(COOLING_FANS_GPIO, 1)
            elif (temperature <= MIN_TEMPERATURE) and is_cooling:
                logging.info("False: Temp={0:0.1f}*C  Humidity={1:0.1f}%".format(temperature, humidity))
                is_cooling = False
                GPIO.output(COOLING_FANS_GPIO, 0)
        else:
            logging.info("Failed to retrieve data from humidity sensor")

        time.sleep(1)


def temperature_sender():
    global humidity
    global temperature
    global client
    global client_lock

    while True:
        with client_lock:
            client.send("TEMP#{}#{}".format(temperature, humidity))
        time.sleep(10)


####Endof-threads####
def main():
    global rfdevice
    global client
    client.connect()
    _thread.start_new_thread(client_recv_updates, ())
    _thread.start_new_thread(wireless_recv, ())
    _thread.start_new_thread(physical_recv, ())
    _thread.start_new_thread(temperature_handler, ())
    _thread.start_new_thread(temperature_sender, ())
    signal.signal(signal.SIGINT, signal_handler)
    while True:
        time.sleep(0.1)


if __name__ == "__main__":
    main()
